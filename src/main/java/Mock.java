import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.matching.ContentPattern;
import com.github.tomakehurst.wiremock.matching.MatchResult;
import com.github.tomakehurst.wiremock.matching.StringValuePattern;

import java.util.HashMap;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;

public class Mock {

    public static void main(String[] args) {
        WireMockServer wireMockServer;

        wireMockServer = new WireMockServer(options().port(8085));
        wireMockServer.start();

        ResponseDefinitionBuilder res = new ResponseDefinitionBuilder();
        res.withStatus(200);
        String response = "{ \"balance\": 3000}";
        res.withBody(response);

        res.withHeader("Content-type", "application/json");

        WireMock.configureFor("localhost", 8085);

        WireMock.stubFor(
                WireMock.post("/balance")
                        .withRequestBody(WireMock.containing("41111"))
                        .willReturn(res)
        );

        ResponseDefinitionBuilder negativeRes = new ResponseDefinitionBuilder();
        negativeRes.withStatus(200);
        negativeRes.withHeader("Content-type", "application/json");
        String negativeResponse = "{ \"balance\": 1}";
        negativeRes.withBody(negativeResponse);

        WireMock.stubFor(
                WireMock.post("/balance")
                        .withRequestBody(WireMock.matchingJsonPath("$.creditCard", WireMock.equalTo("5111111111111111")))
                        .willReturn(negativeRes)
        );

    }


}
